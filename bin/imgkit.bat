::call imgkit magiskpatch boot文件完整路径 新boot文件路径 24100,25200或面具apk路径                       noprompt(可选)
::            recinst     boot文件完整路径 新boot文件路径 recovery文件完整路径(可以是img或ramdisk.cpio)   noprompt(可选)

@ECHO OFF
set var1=%1& set var2=%2& set var3=%3& set var4=%4& set var5=%5& set var6=%6& set var7=%7& set var8=%8& set var9=%9
goto %var1%


:RECINST
SETLOCAL
set logger=imgkit.bat-recinst
set bootpath=%var2%& set outputpath=%var3%& set recpath=%var4%& set mode=%var5%
call log %logger% I 接收变量:bootpath:%bootpath%.outputpath:%outputpath%.recpath:%recpath%.mode:%noprompt%
:RECINST-1
::检查是否存在
if not exist %bootpath% ECHOC {%c_e%}找不到%bootpath%{%c_i%}{\n}& call log %logger% F 找不到%bootpath%& goto FATAL
if not exist %recpath% ECHOC {%c_e%}找不到%recpath%{%c_i%}{\n}& call log %logger% F 找不到%recpath%& goto FATAL
if not "%mode%"=="noprompt" (if exist %outputpath% ECHOC {%c_w%}已存在%outputpath%, 继续将覆盖此文件. {%c_h%}按任意键继续...{%c_i%}{\n}& call log %logger% W 已存在%outputpath%.继续将覆盖此文件& pause>nul & ECHO.继续...)
::准备环境
call log %logger% I 准备环境
if exist tmp\imgkit-recinst rd /s /q tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}删除tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% E 删除tmp\imgkit-recinst失败
md tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}创建tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% E 创建tmp\imgkit-recinst失败
::判断rec文件格式
for %%i in ("%recpath%") do (if not "%%~xi"==".img" goto RECINST-2)
::如果是img则解包提取ramdisk
call log %logger% I 所选recovery是img文件.开始解包提取
cd tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}进入tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% F 进入tmp\imgkit-recinst失败&& goto FATAL
magiskboot.exe unpack %recpath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}解包%recpath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 解包%recpath%失败&& pause>nul && ECHO.重试... && cd %framwork_workspace% && goto RECINST-1
cd %framwork_workspace% 1>nul 2>>%logfile% || ECHOC {%c_e%}进入%framwork_workspace%失败{%c_i%}{\n}&& call log %logger% F 进入%framwork_workspace%失败&& goto FATAL
if not exist tmp\imgkit-recinst\ramdisk.cpio ECHOC {%c_e%}找不到tmp\imgkit-recinst\ramdisk.cpio, 解包%recpath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% E 找不到tmp\imgkit-recinst\ramdisk.cpio.解包%recpath%失败& pause>nul & ECHO.重试... & goto RECINST-1
move /Y tmp\imgkit-recinst\ramdisk.cpio tmp\imgkit-recinst\ramdisk.cpio_new 1>>%logfile% 2>&1 || ECHOC {%c_e%}重命名tmp\imgkit-recinst\ramdisk.cpio失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 重命名tmp\imgkit-recinst\ramdisk.cpio失败&& pause>nul && ECHO.重试... && goto RECINST-1
call log %logger% I 提取ramdisk.cpio完毕
goto RECINST-3
:RECINST-2
::如果不是img则直接复制为ramdisk.cpio_new
echo.F|xcopy /Y %recpath% tmp\imgkit-recinst\ramdisk.cpio_new 1>>%logfile% 2>&1 || ECHOC {%c_e%}复制%recpath%到tmp\imgkit-recinst\ramdisk.cpio_new失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 复制%recpath%到tmp\imgkit-recinst\ramdisk.cpio_new失败&& pause>nul && ECHO.重试... && goto RECINST-1
goto RECINST-3
:RECINST-3
::解包boot
call log %logger% I 开始解包boot
cd tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}进入tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% F 进入tmp\imgkit-recinst失败&& goto FATAL
magiskboot.exe cleanup 1>>%logfile% 2>&1 || ECHOC {%c_e%}magiskboot清理运行环境失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E magiskboot清理运行环境失败&& pause>nul && ECHO.重试... && cd %framwork_workspace% && goto RECINST-3
magiskboot.exe unpack -h %bootpath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}解包%bootpath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 解包%bootpath%失败&& pause>nul && ECHO.重试... && cd %framwork_workspace% && goto RECINST-3
cd %framwork_workspace% 1>nul 2>>%logfile% || ECHOC {%c_e%}进入%framwork_workspace%失败{%c_i%}{\n}&& call log %logger% F 进入%framwork_workspace%失败&& goto FATAL
::cmdline_remove_skip_override
call log %logger% I 开始cmdline_remove_skip_override
if not exist tmp\imgkit-recinst\header ECHOC {%c_e%}找不到tmp\imgkit-recinst\header, 解包%bootpath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% E 找不到tmp\imgkit-recinst\header.解包%bootpath%失败& pause>nul & ECHO.重试... & goto RECINST-3
echo.#!/busybox ash>tmp\imgkit-recinst\cmdline_remove_skip_override.sh
echo.sed -i "s|$(grep '^cmdline=' tmp/imgkit-recinst/header | cut -d= -f2-)|$(grep '^cmdline=' tmp/imgkit-recinst/header | cut -d= -f2- | sed -e 's/skip_override//' -e 's/  */ /g' -e 's/[ \t]*$//')|" tmp/imgkit-recinst/header>>tmp\imgkit-recinst\cmdline_remove_skip_override.sh
::busybox.exe ash tmp\imgkit-recinst\cmdline_remove_skip_override.sh 1>>%logfile% 2>&1 || ECHOC {%c_e%}cmdline_remove_skip_override失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E cmdline_remove_skip_override失败&& pause>nul && ECHO.重试... && goto RECINST-3
busybox.exe ash tmp\imgkit-recinst\cmdline_remove_skip_override.sh 1>>%logfile% 2>&1 || call log %logger% W cmdline_remove_skip_override失败
::hexpatch_kernel
call log %logger% I 开始hexpatch_kernel
cd tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}进入tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% F 进入tmp\imgkit-recinst失败&& goto FATAL
magiskboot.exe hexpatch kernel 77616E745F696E697472616D6673 736B69705F696E697472616D6673 1>>%logfile% 2>&1 || call log %logger% E hexpatch_kernel失败
cd %framwork_workspace% 1>nul 2>>%logfile% || ECHOC {%c_e%}进入%framwork_workspace%失败{%c_i%}{\n}&& call log %logger% F 进入%framwork_workspace%失败&& goto FATAL
::替换ramdisk
call log %logger% I 开始替换ramdisk
move /Y tmp\imgkit-recinst\ramdisk.cpio_new tmp\imgkit-recinst\ramdisk.cpio 1>>%logfile% 2>&1 || ECHOC {%c_e%}替换tmp\imgkit-recinst\ramdisk.cpio失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 替换tmp\imgkit-recinst\ramdisk.cpio失败&& pause>nul && ECHO.重试... && goto RECINST-3
::打包boot
call log %logger% I 开始打包boot
cd tmp\imgkit-recinst 1>nul 2>>%logfile% || ECHOC {%c_e%}进入tmp\imgkit-recinst失败{%c_i%}{\n}&& call log %logger% F 进入tmp\imgkit-recinst失败&& goto FATAL
magiskboot.exe repack %bootpath% boot_new.img 1>>%logfile% 2>&1 || ECHOC {%c_e%}打包boot失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 打包boot失败&& pause>nul && ECHO.重试... && cd %framwork_workspace% && goto RECINST-3
cd %framwork_workspace% 1>nul 2>>%logfile% || ECHOC {%c_e%}进入%framwork_workspace%失败{%c_i%}{\n}&& call log %logger% F 进入%framwork_workspace%失败&& goto FATAL
move /Y tmp\imgkit-recinst\boot_new.img %outputpath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}移动tmp\imgkit-recinst\boot_new.img到%outputpath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 移动tmp\imgkit-recinst\boot_new.img到%outputpath%失败&& pause>nul && ECHO.重试... && goto RECINST-3
call log %logger% I 全部完成
ENDLOCAL
goto :eof



:MAGISKPATCH
SETLOCAL
set logger=imgkit.bat-magiskpatch
set bootpath=%var2%& set outputpath=%var3%& set apkpath=%var4%& set mode=%var5%
if "%apkpath%"=="" set apkpath=tool\Android\magisk\24100.apk
if "%apkpath%"=="24100" set apkpath=tool\Android\magisk\24100.apk
if "%apkpath%"=="25200" set apkpath=tool\Android\magisk\25200.apk
call log %logger% I 接收变量:bootpath:%bootpath%.apkpath:%apkpath%.mode:%noprompt%
::检查是否存在
if not exist %bootpath% ECHOC {%c_e%}找不到%bootpath%{%c_i%}{\n}& call log %logger% F 找不到%bootpath%& goto FATAL
if not exist %apkpath% ECHOC {%c_e%}找不到%apkpath%{%c_i%}{\n}& call log %logger% F 找不到%apkpath%& goto FATAL
if not "%mode%"=="noprompt" (if exist %outputpath% ECHOC {%c_w%}已存在%outputpath%, 继续将覆盖此文件. {%c_h%}按任意键继续...{%c_i%}{\n}& call log %logger% W 已存在%outputpath%.继续将覆盖此文件& pause>nul & ECHO.继续...)
::准备环境
call log %logger% I 准备环境
if exist tmp\imgkit-magiskpatch rd /s /q tmp\imgkit-magiskpatch 1>nul 2>>%logfile% || ECHOC {%c_e%}删除tmp\imgkit-magiskpatch失败{%c_i%}{\n}&& call log %logger% E 删除tmp\imgkit-magiskpatch失败
md tmp\imgkit-magiskpatch 1>nul 2>>%logfile% || ECHOC {%c_e%}创建tmp\imgkit-magiskpatch失败{%c_i%}{\n}&& call log %logger% E 创建tmp\imgkit-magiskpatch失败
copy /Y %bootpath% tmp\imgkit-magiskpatch\boot.img 1>>%logfile% 2>&1 || ECHOC {%c_e%}复制%bootpath%到tmp\imgkit-magiskpatch\boot.img失败{%c_i%}{\n}&& call log %logger% F 复制%bootpath%到tmp\imgkit-magiskpatch\boot.img失败&& goto FATAL
copy /Y %apkpath% tmp\imgkit-magiskpatch\magisk.apk 1>>%logfile% 2>&1 || ECHOC {%c_e%}复制%apkpath%到tmp\imgkit-magiskpatch\magisk.apk失败{%c_i%}{\n}&& call log %logger% F 复制%apkpath%到tmp\imgkit-magiskpatch\magisk.apk失败&& goto FATAL
::开始修补
call log %logger% I 开始修补
cd tmp\imgkit-magiskpatch || ECHOC {%c_e%}进入tmp\imgkit-magiskpatch失败{%c_i%}{\n}&& call log %logger% F 进入tmp\imgkit-magiskpatch失败&& goto FATAL
magiskpatcher.exe -a arm64 magisk.apk 1>>%logfile% 2>&1 || ECHOC {%c_e%}提取magisk.apk失败{%c_i%}{\n}&& call log %logger% F 提取%apkpath%失败&& goto FATAL
magiskpatcher.exe -kv KV -ke KE -d D boot.img 1>>%logfile% 2>&1 || ECHOC {%c_e%}修补boot失败{%c_i%}{\n}&& call log %logger% F 修补%bootpath%失败&& goto FATAL
cd %framwork_workspace% || ECHOC {%c_e%}进入%framwork_workspace%失败{%c_i%}{\n}&& call log %logger% F 进入%framwork_workspace%失败&& goto FATAL
call log %logger% I 修补完成
move /Y tmp\imgkit-magiskpatch\new-boot.img %outputpath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}移动tmp\imgkit-magiskpatch\new-boot.img到%outputpath%失败{%c_i%}{\n}&& call log %logger% F 移动tmp\imgkit-magiskpatch\new-boot.img到%outputpath%失败&& goto FATAL
call log %logger% I 全部完成
ENDLOCAL
goto :eof



:FATAL
ECHO. & if exist tool\Windows\ECHOC.exe (tool\Windows\ECHOC {%c_e%}抱歉, 脚本遇到问题, 无法继续运行. 请查看日志. {%c_h%}按任意键退出...{%c_i%}{\n}& pause>nul & EXIT) else (ECHO.抱歉, 脚本遇到问题, 无法继续运行. 按任意键退出...& pause>nul & EXIT)
