::call choice common     [1][2][3][4][5]#[A][B](可选)
::            twochoice  说明文字(不能带空格)

@ECHO OFF
set var1=%1& set var2=%2& set var3=%3& set var4=%4& set var5=%5& set var6=%6& set var7=%7& set var8=%8& set var9=%9
goto %var1%


:COMMON
SETLOCAL
::检查和获取默认选项
set choice_default=
echo.%var2% | find "#[" 1>nul 2>nul || goto COMMON-1
echo.%var2% | find "]#[" 1>nul 2>nul && goto COMMON-2
goto COMMON-3
:COMMON-2
for /f "tokens=2 delims=# " %%a in ('echo.%var2%') do set var=%%a
goto COMMON-4
:COMMON-3
for /f "tokens=1 delims=# " %%a in ('echo.%var2%') do set var=%%a
goto COMMON-4
:COMMON-4
for /f "tokens=1 delims=[]" %%a in ('echo.%var%') do set choice_default=%%a
::用户输入
:COMMON-1
set choice=
if "%choice_default%"=="" (ECHOC {%c_h%}输入序号按Enter继续: {%c_i%}& set /p choice=) else (ECHOC {%c_h%}输入序号按Enter继续^(默认:%choice_default%^): {%c_i%}& set /p choice=)
echo.%choice% | find "[" 1>nul 2>nul && ECHOC {%c_e%}输入错误, 输入的内容不应带有[]符号. 请重新输入.{%c_i%}{\n}&& goto COMMON
echo.%choice% | find "]" 1>nul 2>nul && ECHOC {%c_e%}输入错误, 输入的内容不应带有[]符号. 请重新输入.{%c_i%}{\n}&& goto COMMON
echo.%choice% | find "#" 1>nul 2>nul && ECHOC {%c_e%}输入错误, 输入的内容不应带有#符号. 请重新输入.{%c_i%}{\n}&& goto COMMON
if "%choice%"=="a" set choice=A
if "%choice%"=="b" set choice=B
if "%choice%"=="c" set choice=C
if "%choice%"=="d" set choice=D
if "%choice%"=="e" set choice=E
if "%choice%"=="f" set choice=F
if "%choice%"=="g" set choice=G
if "%choice%"=="h" set choice=H
if "%choice%"=="i" set choice=I
if "%choice%"=="j" set choice=J
if "%choice%"=="k" set choice=K
if "%choice%"=="l" set choice=L
if "%choice%"=="m" set choice=M
if "%choice%"=="n" set choice=N
if "%choice%"=="o" set choice=O
if "%choice%"=="p" set choice=P
if "%choice%"=="q" set choice=Q
if "%choice%"=="r" set choice=R
if "%choice%"=="s" set choice=S
if "%choice%"=="t" set choice=T
if "%choice%"=="u" set choice=U
if "%choice%"=="v" set choice=V
if "%choice%"=="w" set choice=W
if "%choice%"=="x" set choice=X
if "%choice%"=="y" set choice=Y
if "%choice%"=="z" set choice=Z
if not "%var2%"=="" (if not "%choice_default%"=="" (if "%choice%"=="" set choice=%choice_default%))
if not "%var2%"=="" echo.%var2% | find "[%choice%]" 1>nul 2>nul || ECHOC {%c_e%}输入错误, 输入的选项不在%var2%中. 请重新输入.{%c_i%}{\n}&& goto COMMON
call log choice.bat-common I 选择%choice%
ENDLOCAL & set choice=%choice%
goto :eof



:TWOCHOICE
set choice=
ECHOC {%c_h%}& set /p choice=%var2%& ECHOC {%c_i%}
if "%choice%"=="1" call log choice.bat-twochoice I 选择%choicw%& goto :eof
if "%choice%"=="" call log choice.bat-twochoice I 选择%choicw%& goto :eof
ECHOC {%c_e%}输入错误, 请重新输入.{%c_i%}{\n}& goto TWOCHOICE







:FATAL
ECHO. & if exist tool\Windows\ECHOC.exe (tool\Windows\ECHOC {%c_e%}抱歉, 脚本遇到问题, 无法继续运行. 请查看日志. {%c_h%}按任意键退出...{%c_i%}{\n}& pause>nul & EXIT) else (ECHO.抱歉, 脚本遇到问题, 无法继续运行. 按任意键退出...& pause>nul & EXIT)
