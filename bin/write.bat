::call write system        分区名        img路径
::           recovery      分区名        img路径
::           fastboot      分区名        img路径
::           fastbootboot  img路径
::           edl           端口号        存储类型     img所在文件夹                                           xml路径  firehose完整路径(可选,不填不发送)
::           twrpinst      zip路径
::           sideload      zip路径
::           adbpush       源文件路径    推送后文件名  [common program program_su](文件类型,可选,默认为common)


@ECHO OFF
set var1=%1& set var2=%2& set var3=%3& set var4=%4& set var5=%5& set var6=%6& set var7=%7& set var8=%8& set var9=%9
goto %var1%


:ADBPUSH
SETLOCAL
set logger=write.bat-adbpush
::接收变量
set filepath=%var2%& set pushname_full=%var3%& set mode=%var4%
call log %logger% I 接收变量:filepath:%filepath%.pushname_full:%pushname_full%.mode:%mode%
:ADBPUSH-1
::检查是否存在
if not exist %filepath% ECHOC {%c_e%}找不到%filepath%{%c_i%}{\n}& call log %logger% F 找不到%filepath%& goto FATAL
::获取文件名(不包括扩展名)
for %%i in ("%pushname_full%") do set pushname=%%~ni
::获取文件扩展名
for %%i in ("%pushname_full%") do set var=%%~xi
set pushname_ext=%var:~1,999%
::计算文件大小(b)
for /f "tokens=5 delims= " %%i in ('busybox.exe ls -l %filepath%') do set filesize_b=%%i
call log %logger% I %filepath%大小为%filesize_b%b
::检查设备模式
call chkdev all 1>nul
if not "%chkdev__all__mode%"=="system" (if not "%chkdev__all__mode%"=="recovery" ECHOC {%c_e%}设备模式错误. {%c_i%}请将设备进入系统或Recovery模式. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% E 设备模式错误& pause>nul & ECHO.重试... & goto ADBPUSH-1)
goto ADBPUSH-%chkdev__all__mode%
:ADBPUSH-SYSTEM
::系统下分两种:普通推sdcard,程序推data/local/tmp
::无论哪种都需要首先推送到sdcard
set pushfolder=./sdcard
call log %logger% I 推送%filepath%到%pushfolder%/%pushname_full%
adb.exe push %filepath% %pushfolder%/%pushname_full% 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%filepath%到%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%filepath%到%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM
call log %logger% I 检查%pushfolder%/%pushname_full%大小
adb.exe shell wc -c %pushfolder%/%pushname_full% 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}获取%pushfolder%/%pushname_full%大小失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 获取%pushfolder%/%pushname_full%大小失败&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM
set filesize_pushed_b=
for /f "tokens=1 delims= " %%i in (tmp\output.txt) do set filesize_pushed_b=%%i
call log %logger% I %pushfolder%/%pushname_full%大小为%filesize_pushed_b%b
if not "%filesize_b%"=="%filesize_pushed_b%" ECHOC {%c_e%}已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM
if "%mode:~0,7%"=="program" (
    ::对于程序要移动到./data/local/tmp以便执行
    set pushfolder=./data/local/tmp
    call log %logger% I 移动./sdcard/%pushname_full%到./data/local/tmp
    adb.exe shell mv -f ./sdcard/%pushname_full% ./data/local/tmp 1>>%logfile% 2>&1 || ECHOC {%c_e%}移动./sdcard/%pushname_full%到./data/local/tmp失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 移动./sdcard/%pushname_full%到./data/local/tmp失败&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM)
if not "%mode%"=="program_su" goto ADBPUSH-DONE
:ADBPUSH-SYSTEM-1
::对于需要授权的程序则授权
ECHOC {%c_we%}正在申请Root权限. 请注意手机提示. 或在Root权限管理页面手动授权Shell...{%c_i%}{\n}& call log %logger% I 检查Root权限
echo.su>tmp\cmd.txt & echo.whoami>>tmp\cmd.txt
adb.exe shell < tmp\cmd.txt 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}检查用户失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& type tmp\output.txt>>%logfile% && call log %logger% E 检查用户失败失败&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM-1
type tmp\output.txt>>%logfile%
for /f %%a in (tmp\output.txt) do (if not "%%a"=="root" ECHOC {%c_e%}检查Root权限失败. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% E 检查Root权限失败& pause>nul & ECHO.重试... & goto ADBPUSH-SYSTEM-1)
call log %logger% I 授权%pushfolder%/%pushname_full%
echo.su>tmp\cmd.txt & echo.chmod +x %pushfolder%/%pushname_full%>>tmp\cmd.txt
adb.exe shell < tmp\cmd.txt 1>>%logfile% 2>&1 || ECHOC {%c_e%}授权%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 授权%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-SYSTEM-1
goto ADBPUSH-DONE
::Recovery下
:ADBPUSH-RECOVERY
call log %logger% I 检查Root权限
adb.exe shell whoami 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}检查用户失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& type tmp\output.txt>>%logfile% && call log %logger% E 检查用户失败失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY
type tmp\output.txt>>%logfile%
for /f %%a in (tmp\output.txt) do (if not "%%a"=="root" ECHOC {%c_e%}检查Root权限失败. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% E 检查Root权限失败& pause>nul & ECHO.重试... & goto ADBPUSH-RECOVERY)
if "%mode:~0,7%"=="program" goto ADBPUSH-RECOVERY-1
goto ADBPUSH-RECOVERY-2
::如果是程序直接推根目录
:ADBPUSH-RECOVERY-1
set pushfolder=.
call log %logger% I 推送%filepath%到%pushfolder%/%pushname_full%
adb.exe push %filepath% %pushfolder%/%pushname_full% 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%filepath%到%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%filepath%到%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-1
call log %logger% I 检查%pushfolder%/%pushname_full%大小
adb.exe shell wc -c %pushfolder%/%pushname_full% 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}获取%pushfolder%/%pushname_full%大小失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 获取%pushfolder%/%pushname_full%大小失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-1
set filesize_pushed_b=
for /f "tokens=1 delims= " %%i in (tmp\output.txt) do set filesize_pushed_b=%%i
call log %logger% I %pushfolder%/%pushname_full%大小为%filesize_pushed_b%b
if not "%filesize_b%"=="%filesize_pushed_b%" ECHOC {%c_e%}已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-1
if "%mode%"=="program_su" (
    call log %logger% I 授权%pushfolder%/%pushname_full%
    adb.exe shell chmod +x %pushfolder%/%pushname_full% || ECHOC {%c_e%}授权%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 授权%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-1)
goto ADBPUSH-DONE
::推普通文件
::先尝试./tmp
:ADBPUSH-RECOVERY-2
set pushfolder=./tmp
call log %logger% I 尝试删除已有%pushfolder%/%pushname_full%.若文件不存在则报错属于正常现象
adb.exe shell rm %pushfolder%/%pushname_full% 1>>%logfile% 2>&1
call log %logger% I 检查./tmp
adb.exe shell df -a -k 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}检查挂载失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& type tmp\output.txt>>%logfile% && call log %logger% E 检查挂载失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-2
type tmp\output.txt>>%logfile%
busybox.exe sed -i "s/$/& /g" tmp\output.txt 1>>%logfile% 2>&1 || ECHOC {%c_e%}sed处理tmp\output.txt失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E sed处理tmp\output.txt失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-2
set availsize=
for /f "tokens=4 delims= " %%i in ('find " /tmp " "tmp\output.txt"') do set availsize=%%i
if "%availsize%"=="" call log %logger% W ./tmp未挂载& goto ADBPUSH-RECOVERY-4
call calc kb2b availsize_b nodec %availsize%
call log %logger% I ./tmp可用空间为%availsize_b%b
for /f %%a in ('numcomp.exe %availsize_b% %filesize_b%') do (if "%%a"=="less" call log %logger% W ./tmp空间不足& goto ADBPUSH-RECOVERY-4)
adb.exe shell mkdir -p ./tmp/bff-test 1>>%logfile% 2>&1 || call log %logger% W 创建./tmp/bff-test失败&& goto ADBPUSH-RECOVERY-4
adb.exe shell ls -l ./tmp | find " bff-test" 1>nul 2>nul || call log %logger% W 找不到./tmp/bff-test&& goto ADBPUSH-RECOVERY-4
adb.exe shell rm -rf ./tmp/bff-test 1>>%logfile% 2>&1 || call log %logger% W 删除./tmp/bff-test失败&& goto ADBPUSH-RECOVERY-4
call log %logger% I 检查./tmp完毕.开始推送%filepath%到%pushfolder%/%pushname_full%
:ADBPUSH-RECOVERY-3
adb.exe push %filepath% %pushfolder%/%pushname_full% 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%filepath%到%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%filepath%到%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-3
call log %logger% I 检查%pushfolder%/%pushname_full%大小
adb.exe shell wc -c %pushfolder%/%pushname_full% 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}获取%pushfolder%/%pushname_full%大小失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 获取%pushfolder%/%pushname_full%大小失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-3
set filesize_pushed_b=
for /f "tokens=1 delims= " %%i in (tmp\output.txt) do set filesize_pushed_b=%%i
call log %logger% I %pushfolder%/%pushname_full%大小为%filesize_pushed_b%b
if not "%filesize_b%"=="%filesize_pushed_b%" ECHOC {%c_e%}已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-3
goto ADBPUSH-DONE
::再尝试./data. data是最后的选项.
:ADBPUSH-RECOVERY-4
set pushfolder=./data
call log %logger% I 尝试删除已有%pushfolder%/%pushname_full%.若文件不存在则报错属于正常现象
adb.exe shell rm %pushfolder%/%pushname_full% 1>>%logfile% 2>&1
call log %logger% I 检查./data
adb.exe shell df -a -k 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}检查挂载失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& type tmp\output.txt>>%logfile% && call log %logger% E 检查挂载失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-4
type tmp\output.txt>>%logfile%
busybox.exe sed -i "s/$/& /g" tmp\output.txt 1>>%logfile% 2>&1 || ECHOC {%c_e%}sed处理tmp\output.txt失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E sed处理tmp\output.txt失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-4
set availsize=
for /f "tokens=4 delims= " %%i in ('find " /data " "tmp\output.txt"') do set availsize=%%i
if "%availsize%"=="" ECHOC {%c_e%}./data未挂载. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% W ./data未挂载& pause>nul & ECHO.重试... & goto ADBPUSH-RECOVERY-4
call calc kb2b availsize_b nodec %availsize%
call log %logger% I ./data可用空间为%availsize_b%b
for /f %%a in ('numcomp.exe %availsize_b% %filesize_b%') do (if "%%a"=="less" ECHOC {%c_e%}./data空间不足. {%c_h%}按任意键重试...{%c_i%}{\n}& call log %logger% W ./data空间不足& pause>nul & ECHO.重试... & goto ADBPUSH-RECOVERY-4)
adb.exe shell mkdir -p ./data/bff-test 1>>%logfile% 2>&1 || ECHOC {%c_e%}./data读写测试失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% W 创建./data/bff-test失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-4
adb.exe shell ls -l ./data | find " bff-test" 1>nul 2>nul || ECHOC {%c_e%}./data读写测试失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% W 找不到./data/bff-test&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-4
adb.exe shell rm -rf ./data/bff-test 1>>%logfile% 2>&1 || ECHOC {%c_e%}./data读写测试失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% W 删除./data/bff-test失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-4
call log %logger% I 检查./data完毕.开始推送%filepath%到%pushfolder%/%pushname_full%
:ADBPUSH-RECOVERY-5
adb.exe push %filepath% %pushfolder%/%pushname_full% 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%filepath%到%pushfolder%/%pushname_full%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%filepath%到%pushfolder%/%pushname_full%失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-5
call log %logger% I 检查%pushfolder%/%pushname_full%大小
adb.exe shell wc -c %pushfolder%/%pushname_full% 1>tmp\output.txt 2>&1 || ECHOC {%c_e%}获取%pushfolder%/%pushname_full%大小失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 获取%pushfolder%/%pushname_full%大小失败&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-5
set filesize_pushed_b=
for /f "tokens=1 delims= " %%i in (tmp\output.txt) do set filesize_pushed_b=%%i
call log %logger% I %pushfolder%/%pushname_full%大小为%filesize_pushed_b%b
if not "%filesize_b%"=="%filesize_pushed_b%" ECHOC {%c_e%}已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 已推送文件大小%filesize_pushed_b%b和原文件大小%filesize_b%b不一致&& pause>nul && ECHO.重试... && goto ADBPUSH-RECOVERY-5
goto ADBPUSH-DONE
:ADBPUSH-DONE
call log %logger% I ADB推送完毕.文件路径:%pushfolder%/%pushname_full%
ENDLOCAL & set write__adbpush__filepath=%pushfolder%/%pushname_full%& set write__adbpush__filename_full=%pushname_full%& set write__adbpush__filename=%pushname%& set write__adbpush__folder=%pushfolder%& set write__adbpush__ext=%pushname_ext%
goto :eof


:SIDELOAD
SETLOCAL
set logger=write.bat-sideload
::接收变量
set zippath=%var2%
call log %logger% I 接收变量:zippath:%zippath%
:SIDELOAD-1
::检查是否存在
if not exist %zippath% ECHOC {%c_e%}找不到%zippath%{%c_i%}{\n}& call log %logger% F 找不到%zippath%& goto FATAL
::安装
call reboot recovery sideload rechk 3
call log %logger% I 安装%zippath%
adb.exe sideload %zippath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}安装%zippath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 安装%zippath%失败&& pause>nul && ECHO.重试... && goto SIDELOAD-1
ENDLOCAL
goto :eof


:TWRPINST
SETLOCAL
set logger=write.bat-twrpinst
::接收变量
set zippath=%var2%
call log %logger% I 接收变量:zippath:%zippath%
:TWRPINST-1
::检查是否存在
if not exist %zippath% ECHOC {%c_e%}找不到%zippath%{%c_i%}{\n}& call log %logger% F 找不到%zippath%& goto FATAL
::推送
call log %logger% I 推送%zippath%
call write adbpush %zippath% bff.zip common
::adb.exe push %zippath% ./tmp/bff.zip 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%zippath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%zippath%失败&& pause>nul && ECHO.重试... && goto TWRPINST-1
::安装
call log %logger% I 安装%write__adbpush__filepath%
adb.exe shell twrp install %write__adbpush__filepath% 1>tmp\output.txt 2>&1 || type tmp\output.txt>>%logfile% && call log %logger% E 安装%zippath%失败&& ECHOC {%c_e%}安装%zippath%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& pause>nul && ECHO.重试... && goto TWRPINST-1
type tmp\output.txt>>%logfile%
find "zip" "tmp\output.txt" 1>nul 2>nul || ECHOC {%c_e%}安装%zippath%失败, TWRP未执行命令. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 安装%zippath%失败,TWRP未执行命令&& pause>nul && ECHO.重试... && goto TWRPINST-1
adb.exe shell rm %write__adbpush__filepath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}删除%write__adbpush__filepath%失败{%c_i%}{\n}&& call log %logger% E 删除%write__adbpush__filepath%失败
ENDLOCAL
goto :eof


:EDL
SETLOCAL
set logger=write.bat-edl
::接收变量
set port=%var2%& set memory=%var3%& set searchpath=%var4%& set xml=%var5%& set fh=%var6%
call log %logger% I 接收变量:port:%port%.memory:%memory%.searchpath:%searchpath%.xml:%xml%.fh:%fh%
:EDL-1
::检查img和firehose是否存在
if not exist %searchpath% ECHOC {%c_e%}找不到%searchpath%{%c_i%}{\n}& call log %logger% F 找不到%searchpath%& goto FATAL
if not "%fh%"=="" (if not exist %fh% ECHOC {%c_e%}找不到%fh%{%c_i%}{\n}& call log %logger% F 找不到%fh%& goto FATAL)
::处理xml
echo.%xml%>tmp\output.txt
for /f %%a in ('busybox.exe sed "s/\//,/g" tmp\output.txt') do set xml=%%a
call log %logger% I xml参数更新为:
echo.%xml%>>%logfile%
::发送引导
if not "%fh%"=="" (
    ECHO.正在发送引导...& call log %logger% I 正在发送引导
    QSaharaServer.exe -p \\.\COM%port% -s 13:%fh% 1>>%logfile% 2>&1 || ECHOC {%c_e%}发送引导失败.{%c_h%}按任意键继续...{%c_i%}{\n}&& call log %logger% E 发送引导失败&& pause>nul)
::开始刷机
ECHO.正在9008刷入...& call log %logger% I 正在9008刷入
fh_loader.exe --port=\\.\COM%port% --memoryname=%memory% --search_path=%searchpath% --sendxml=%xml% --noprompt 1>>%logfile% 2>&1 || ECHOC {%c_e%}9008刷入失败.{%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 9008刷入失败&& move /Y port_trace.txt log 1>>%logfile% 2>&1 && pause>nul && ECHO.重试... && goto EDL-1
::--showpercentagecomplete --reset --noprompt --zlpawarehost=1 --testvipimpact
move /Y port_trace.txt log 1>>%logfile% 2>&1
ECHO.9008刷入完成& call log %logger% I 9008刷入完成
ENDLOCAL
goto :eof


:SYSTEM
SETLOCAL
set logger=write.bat-system
set target=system
goto ADBDD


:RECOVERY
SETLOCAL
set logger=write.bat-recovery
set target=recovery
goto ADBDD


:ADBDD
::接收变量
set parname=%var2%& set imgpath=%var3%
call log %logger% I 接收变量:parname:%parname%.imgpath:%imgpath%
:ADBDD-1
::检查文件
if not exist %imgpath% ECHOC {%c_e%}找不到%imgpath%{%c_i%}{\n}& call log %logger% F 找不到%imgpath%& goto FATAL
::系统下要检查Root
if "%target%"=="system" (
    call log %logger% I 开始检查Root
    echo.su>tmp\cmd.txt& echo.exit>>tmp\cmd.txt& echo.exit>>tmp\cmd.txt
    adb.exe shell < tmp\cmd.txt 1>>%logfile% 2>&1 || ECHOC {%c_e%}获取Root失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 获取Root失败&& pause>nul && ECHO.重试... && goto ADBDD-1)
::推送
call log %logger% I 开始推送%imgpath%
call write adbpush %imgpath% %parname%.img common
::adb.exe push %imgpath% %target%/%parname%.img 1>>%logfile% 2>&1 || ECHOC {%c_e%}推送%imgpath%到%target%/%parname%.img失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 推送%imgpath%到%target%/%parname%.img失败&& pause>nul && ECHO.重试... && goto ADBDD-1
::获取分区路径
call info par %parname%
::刷入和清理
if "%target%"=="system" echo.su>tmp\cmd.txt& echo.dd if=%write__adbpush__filepath% of=%info__par__path% >>tmp\cmd.txt& echo.rm %write__adbpush__filepath%>>tmp\cmd.txt
if "%target%"=="recovery" echo.dd if=%write__adbpush__filepath% of=%info__par__path% >tmp\cmd.txt& echo.rm %write__adbpush__filepath%>>tmp\cmd.txt
call log %logger% I 开始刷入%write__adbpush__filepath%到%info__par__path%
adb.exe shell < tmp\cmd.txt 1>>%logfile% 2>&1 || ECHOC {%c_e%}刷入%write__adbpush__filepath%到%info__par__path%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 刷入%write__adbpush__filepath%到%info__par__path%失败&& pause>nul && ECHO.重试... && goto ADBDD-1
ENDLOCAL
goto :eof


:FASTBOOT
SETLOCAL
set logger=write.bat-fastboot
::接收变量
set parname=%var2%& set imgpath=%var3%
call log %logger% I 接收变量:parname:%parname%.imgpath:%imgpath%
:FASTBOOT-1
::检查文件
if not exist %imgpath% ECHOC {%c_e%}找不到%imgpath%{%c_i%}{\n}& call log %logger% F 找不到%imgpath%& goto FATAL
::刷入
call log %logger% I 开始刷入%imgpath%到%parname%
fastboot.exe flash %parname% %imgpath% 1>>%logfile% 2>&1 || ECHOC {%c_e%}刷入%imgpath%到%parname%失败. {%c_h%}按任意键重试...{%c_i%}{\n}&& call log %logger% E 刷入%imgpath%到%parname%失败&& pause>nul && ECHO.重试... && goto FASTBOOT-1
ENDLOCAL
goto :eof


:FASTBOOTBOOT
SETLOCAL
set logger=write.bat-fastbootboot
::接收变量
set imgpath=%var2%
call log %logger% I 接收变量:imgpath:%imgpath%
:FASTBOOTBOOT-1
::检查文件
if not exist %imgpath% ECHOC {%c_e%}找不到%imgpath%{%c_i%}{\n}& call log %logger% F 找不到%imgpath%& goto FATAL
::临时启动
call log %logger% I 启动%imgpath%
fastboot.exe boot %imgpath% 1>>%logfile% 2>&1 && goto FASTBOOTBOOT-DONE
ECHOC {%c_e%}启动%imgpath%失败{%c_i%}{\n}& call log %logger% E 启动%imgpath%失败
ECHO.1.设备没有进入目标模式, 重新尝试临时启动
ECHO.2.脚本判断有误, 设备已进入目标模式, 可以继续
call choice common [1][2]
if "%choice%"=="2" goto FASTBOOTBOOT-DONE
call chkdev fastboot
ECHO.重新尝试临时启动...
goto FASTBOOTBOOT-1
:FASTBOOTBOOT-DONE
ENDLOCAL
goto :eof








:FATAL
ECHO. & if exist tool\Windows\ECHOC.exe (tool\Windows\ECHOC {%c_e%}抱歉, 脚本遇到问题, 无法继续运行. 请查看日志. {%c_h%}按任意键退出...{%c_i%}{\n}& pause>nul & EXIT) else (ECHO.抱歉, 脚本遇到问题, 无法继续运行. 按任意键退出...& pause>nul & EXIT)

